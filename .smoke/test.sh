#!/bin/bash

#load smoke.sh
. ".smoke/smoke.sh"

# start app and wait till its started
docker run --name test -d -p 3000:80 test-server
sleep 3

# tests
smoke_url_ok "http://localhost:3000"
  smoke_assert_body "Hello *"

# clean up
docker stop test
docker rm test

smoke_report
