## fixed

dind starts containers at its host `docker`, however you can alias it (e.g. to `localhost`) see yaml.

# Smoke test is failing in gitlab runner

Demonstration of https://gitlab.com/gitlab-org/gitlab-runner/issues/6323

Test locally

```
$ docker build -t test-server .
$ .smoke/test.sh                                               [0m]
9565229235d1dccfba3bbf884a95080c2b5dfba5629e4417f300356d2935a107
> http://MacBookPro.local:3000
    [ OK ] 2xx Response code
    [ OK ] Body contains "Hello *"
test
test
OK (2/2)
```

smoke test on gitlab:
```
> http://runner-72989761-project-16323200-concurrent-0:3000
     [FAIL] 2xx Response code
     [FAIL] Body does not contain "Hello *"
     
```
